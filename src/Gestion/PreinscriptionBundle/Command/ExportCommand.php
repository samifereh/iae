<?php
namespace Gestion\PreinscriptionBundle\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Sensio\Bundle\GeneratorBundle\Command\Helper\DialogHelper;

use Gestion\PreinscriptionBundle\Entity\Preinscrit;
use Gestion\PreinscriptionBundle\Entity\Etudiant;
use Gestion\PreinscriptionBundle\Entity\Parents;
use Gestion\NiveauBundle\Entity\Niveau;
use Gestion\FiliereBundle\Entity\Filiere;
use Gestion\PreinscriptionBundle\Form\EtudiantType;
use Gestion\PreinscriptionBundle\Form\EtudiantEditType;
use Gestion\PreinscriptionBundle\Form\ParentsType;
use Gestion\PreinscriptionBundle\Form\ParentsEditType;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
class  ExportCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('export:etudiants')

            // the short description shown while running "php bin/console list"
            ->setDescription('Export list of students.')

            ->addArgument('file', InputArgument::REQUIRED, 'The path for the file.')
            //->addOption("path")

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to export the list of students...')
        ;

    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {

    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //$options = $input->getOptions();
        //dump($options);
        //die;
        $prefix = $input->getArgument('file');
        if ($prefix!='file') {
            $output->writeln(sprintf('The command line should be prefixed by the prefix : file'));
            return;
        }
        else{

        $csvPath = 'web/uploads';
        $helper = $this->getHelper('question');
        $question = new Question('Please select the path name   ', 'path');
        $question1 = new Question('Please enter the name of the file   ', 'file name');
        $path = $helper->ask($input, $output, $question);
        $file_name = $helper->ask($input, $output, $question1);
        //$output->writeln('Argument: '.$input->getArgument('file'));
        $output->writeln('You have just selected the path: '.$path);
        $output->writeln('Your file name is: '.$file_name);
        if($path!=''){
            $exportService = $this->getContainer()->get('iae.export.liste');
            $csvPath = $path.'/'.$file_name;
            $return = $exportService->exportList($csvPath);
        }
        }
    }

}