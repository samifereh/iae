<?php
namespace Gestion\PreinscriptionBundle\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Sensio\Bundle\GeneratorBundle\Command\Helper\DialogHelper;

use Gestion\PreinscriptionBundle\Entity\Preinscrit;
use Gestion\PreinscriptionBundle\Entity\Etudiant;
use Gestion\PreinscriptionBundle\Entity\Parents;
use Gestion\NiveauBundle\Entity\Niveau;
use Gestion\FiliereBundle\Entity\Filiere;
use Gestion\PreinscriptionBundle\Form\EtudiantType;
use Gestion\PreinscriptionBundle\Form\EtudiantEditType;
use Gestion\PreinscriptionBundle\Form\ParentsType;
use Gestion\PreinscriptionBundle\Form\ParentsEditType;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Console\Helper\ProgressBar;

class  ImportCommand extends ContainerAwareCommand
{
    // change these options about the file to read
    private $csvParsingOptions = array(
        'finder_in' => 'web/uploads/',
        'finder_name' => 'liste.csv',
        'ignoreFirstLine' => false);

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('import:preinscrits')

            // the short description shown while running "php bin/console list"
            ->setDescription('Export list of students.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to export the list of students...')
        ;

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // use the parseCSV() function
        $csv = $this->parseCSV();
    }
    /**
     * Parse a csv file
     *
     * @return array
     */
    private function parseCSV()
    {
        $ignoreFirstLine = $this->csvParsingOptions['ignoreFirstLine'];
        $finder = new Finder();
        $finder->files()
            ->in($this->csvParsingOptions['finder_in'])
            ->name($this->csvParsingOptions['finder_name'])
        ;
        foreach ($finder as $file) { $csv = $file; }
        $rows = array();
        if (($handle = fopen($csv->getRealPath(), "r")) !== FALSE) {
            $i = 0;
            while (($data = fgetcsv($handle, null, ";")) !== FALSE) {
                $i++;
                if ($ignoreFirstLine && $i == 1) { continue; }
                $rows[] = $data;
            }

            $em = $this->getContainer()->get('doctrine')->getEntityManager();
            $importService = $this->getContainer()->get('iae.import.liste');
            $return = $importService->ImportList($file);

            fclose($handle);
        }
        return $rows;
    }
}