<?php
namespace Gestion\PreinscriptionBundle\Service;

use Gestion\PreinscriptionBundle\Entity\Preinscrit;
use Gestion\PreinscriptionBundle\Entity\Etudiant;
use Gestion\PreinscriptionBundle\Entity\Parents;
use Gestion\NiveauBundle\Entity\Niveau;
use Gestion\FiliereBundle\Entity\Filiere;
use Gestion\PreinscriptionBundle\Form\EtudiantType;
use Gestion\PreinscriptionBundle\Form\EtudiantEditType;
use Gestion\PreinscriptionBundle\Form\ParentsType;
use Gestion\PreinscriptionBundle\Form\ParentsEditType;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Created by PhpStorm.
 * User: sami
 * Date: 31/07/2017
 * Time: 13:17
 */
class export
{
    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function exportList($csvPath='')
    {

        if($csvPath != "")
            return $this->exportFile($csvPath);

        $response = new StreamedResponse();

        $response->setCallback(function() {

            $this->exportFile('php://output');
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition','attachment; filename="export-etudiants.csv"');

        return $response;
    }

    protected function exportFile($filePath)
    {
        $container = $this->container;
        $em = $container->get('doctrine')->getManager();
        $repository = $em->getRepository('GestionPreinscriptionBundle:Etudiant');
        $handle = fopen($filePath, 'w+');
        fputcsv($handle, ['Nom', 'Prénom', 'Date de Naissance'], ';');

        $results = $repository->findAll();
        foreach ($results as $user) {
            fputcsv(
                $handle,
                [$user->getNom(), $user->getPrenom(), $user->getDateNaissance()->format("Y/m/d")],
                ';'
            );
        }

        fclose($handle);
    }


}