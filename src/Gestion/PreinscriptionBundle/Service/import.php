<?php
namespace Gestion\PreinscriptionBundle\Service;

use Gestion\PreinscriptionBundle\Entity\Preinscrit;
use Gestion\PreinscriptionBundle\Entity\Etudiant;
use Gestion\PreinscriptionBundle\Entity\Parents;
use Gestion\NiveauBundle\Entity\Niveau;
use Gestion\FiliereBundle\Entity\Filiere;
use Gestion\PreinscriptionBundle\Form\EtudiantType;
use Gestion\PreinscriptionBundle\Form\EtudiantEditType;
use Gestion\PreinscriptionBundle\Form\ParentsType;
use Gestion\PreinscriptionBundle\Form\ParentsEditType;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Created by PhpStorm.
 * User: sami
 * Date: 31/07/2017
 * Time: 13:17
 */
class import
{
    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function ImportList($file)
    {
        $handle = fopen($file, "r");
        $stat = [
            "ok" => 0,
            "ko" => 0
                ];
        while(($fileop = fgetcsv($handle,1000, ";")) !== false){
            $entityManager = $this->container->get('doctrine.orm.entity_manager');

            $preinscrit1 = new Preinscrit();
            $preinscrit1->setNom($fileop[0]);
            $preinscrit1->setPrenom($fileop[1]);
            $preinscrit1->setDateNaissance(new \DateTime($fileop[2]));
            $preinscrit1->setLieuNaissance($fileop[3]);
            $preinscrit1->setNationalite($fileop[4]);
            $preinscrit1->setVille($fileop[5]);
            $preinscrit1->setNumCinPass($fileop[6]);
            $preinscrit1->setSexe($fileop[7]);
            $preinscrit1->setAdresse($fileop[8]);
            $preinscrit1->setTel($fileop[9]);
            $preinscrit1->setEmail($fileop[10]);
            $preinscrit1->setDiplome($fileop[11]);
            $preinscrit1->setEtablissement($fileop[12]);
            $preinscrit1->setAnneeObtention(new \DateTime($fileop[13]));
            $preinscrit1->setMessage($fileop[14]);
            $preinscrit1->setNiveau($fileop[15]);
            $preinscrit1->setFormation($fileop[16]);
            $preinscrit1->setEtat("non vérifié");
            $entityManager->persist($preinscrit1);
            $verif = $entityManager->getRepository('GestionPreinscriptionBundle:Preinscrit')->findOneBy(array('numCinPass' => $fileop[6]));
            if($verif){
                echo 'personne déjà existante';
                $stat["ko"]++;
            }else{
                $entityManager->persist($preinscrit1);
                $stat['ok']++;
                $entityManager->flush();
            }
        }
        return $stat;
    }
}