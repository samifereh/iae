<?php

namespace Gestion\PaiementBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\FloatType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PaiementEnseignantType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('enseignant', EntityType::class, array(
                'required' => true,
                'class' => 'GestionEnseignantBundle:Enseignant',
                'placeholder' => '-- Choisir l\'enseignant --',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.id', 'ASC');
                },
                'choice_label' => 'nom',
                'attr' => array(
                    'class'     => 'form-control',
                    'property' => "libCategory", 'multiple' => false, 'expanded' => true
                ),
            ))
            ->add('dateTranche1',DateType::class, array(
                // render as a single text box
                'widget' => 'single_text'))
            ->add('dateTranche2',DateType::class, array(
                // render as a single text box
                'widget' => 'single_text'))
            ->add('dateTranche3',DateType::class, array(
                // render as a single text box
                'widget' => 'single_text'))
            ->add('EtatDateTranche1',ChoiceType::class, array('placeholder'=>'Etat Tranche 1','choices'=> array('Payée'=>'Payée.','Non Payée'=>'Non Payée'),
                'expanded' => true,
                'multiple' => false))
            ->add('EtatDateTranche2',ChoiceType::class, array('placeholder'=>'Etat Tranche 2','choices' => array('Payée'=>'Payée.','Non Payée'=>'Non Payée'),
                'expanded' => true,
                'multiple' => false))
            ->add('EtatDateTranche3',ChoiceType::class, array('placeholder'=>'Etat Tranche 3','choices' => array('Payée'=>'Payée.','Non Payée'=>'Non Payée'),
                'expanded' => true,
                'multiple' => false))
            ->add('montant1',TextType::class, array('attr' => array('class'=>'form-control', 'step'=>'0.01')))
            ->add('montant2',TextType::class, array('attr' => array('class'=>'form-control', 'step'=>'0.01')))
            ->add('montant3',TextType::class, array('attr' => array('class'=>'form-control', 'step'=>'0.01')))
            ->add('submit',SubmitType::class, array('attr' => array('class'=>'btn btn-success')))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gestion\PaiementBundle\Entity\PaiementEnseignant'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gestion_paiementbundle_paiementenseignant';
    }


}
