<?php

namespace Gestion\PaiementBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Gestion\EnseignantBundle\Entity\Enseignant;
use Gestion\PaiementBundle\Entity\PaiementEnseignant;
use Gestion\PaiementBundle\Form\PaiementEnseignantType;
use Gestion\NoteBundle\Entity\Note;
use Gestion\NoteBundle\Form\NoteType;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;
class PaymentEnseignantController extends Controller
{
    public function indexAction()
    {
        return $this->render('GestionPaiementBundle:Default:index.html.twig');
    }

    public function listPaiementEnseignantAction()
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $paiement = $em->getRepository('GestionPaiementBundle:PaiementEnseignant')->findAll();
        $user = $this->getUser();
        return $this->render('GestionPaiementBundle:Default:listePaiementEnseignant.html.twig', array('user' => $user, 'paiements' => $paiement));
    }

    public function infosPaiementEnseignantAction($id)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $paiement = $em->getRepository('GestionPaiementBundle:PaiementEnseignant')->find($id);
        $user = $this->getUser();
        return $this->render('GestionPaiementBundle:Default:infosPaymentEnseignant.html.twig', array('user' => $user, 'paiement' => $paiement));
    }

    public function ajoutPaiementEnseignantAction(Request $request)
    {
        $user = $this->getUser();
        $em = $this->container->get('doctrine')->getEntityManager();
        $paiement= new PaiementEnseignant();
        //on recupere le formulaire
        $form = $this->createForm(PaiementEnseignantType::class,$paiement);
        //on génère le html du formulaire crée
        $formView = $form->createView();
        // Refill the fields in case the form is not valid.
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em->persist($paiement);
            $em->flush();
            return $this->redirect($this->generateUrl('gestion_paiement_enseignant',array('user' => $user)));
        }
        //on rend la vue
        return $this->render('GestionPaiementBundle:Default:ajoutPaiementEnseignant.html.twig',array('user' => $user,
            'form' => $formView));
    }

    public function modifierPaiementEnseignantAction($id, Request $request)
    {
        $user = $this->getUser();
        $em = $this->container->get('doctrine')->getEntityManager();

        $paiement= $this->getDoctrine()->getRepository('GestionPaiementBundle:PaiementEnseignant')->find($id);
        if (null === $paiement) {
            throw new NotFoundHttpException("Pas de paiement d'id ".$id.".");
        }
        //on recupere le formulaire
        $form = $this->createForm(PaiementEnseignantType::class,$paiement);
        //on génère le html du formulaire crée
        $formView = $form->createView();
        // Refill the fields in case the form is not valid.
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $donnee = $form->getData();
            $idEnseignant=$donnee->getEnseignant();
            $dateTranche1=$donnee->getDateTranche1();
            $dateTranch1=$request->get('dateTranche1');
            $dateTranche2=$donnee->getDateTranche2();
            $dateTranch2=$request->get('dateTranche2');
            $dateTranche3=$donnee->getDateTranche3();
            $dateTranch3=$request->get('dateTranche3');
            if (null === $dateTranche1 || null === $dateTranche2 || null === $dateTranche3) {
                $dateTr1 = new \DateTime($dateTranch1);
                $paiement->setDateTranche1($dateTr1);
                $dateTr2 = new \DateTime($dateTranch2);
                $paiement->setDateTranche2($dateTr2);
                $dateTr3 = new \DateTime($dateTranch3);
                $paiement->setDateTranche3($dateTr3);
            }

            $em->persist($paiement);
            $em->flush();
            return $this->redirect($this->generateUrl('gestion_paiement_enseignant',array('user' => $user)));
        }
        //on rend la vue
        return $this->render('GestionPaiementBundle:Default:modifierPaiementEnseignant.html.twig',array('user' => $user, 'form' => $formView,
            'paiement'=>$paiement));
    }

    public function deletePaiementEnseignantAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $note = $em->getRepository('GestionPaiementBundle:PaiementEnseignant')->find($id);
        $em->remove($note);
        $em->flush();
        //on rend la vue
        $usr = $this->getUser();
        return new RedirectResponse($this->container->get('router')->generate('gestion_paiement_enseignant',array('user' => $usr)));
    }


}
